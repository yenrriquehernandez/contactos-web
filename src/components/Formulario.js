import React, { Fragment, useState } from 'react';

const Formulario= () =>{

    //Crear State de Contactos
    const [contacto,actualizarContacto] = useState({
           nombre: '',
           telefono: '',
           email: '',
           direccion: ''
    });

    const actualizarState = () =>{
        console.log('escribiendo...');
    }

    return (
        <Fragment>
            <h2>Crear Cita</h2>

            <form>
                <label> Crear Contacto</label>
                <input
                   type = "text"
                    name = "mascota"
                    className = "u-full-width"
                    placeholder = "Nombre"
                    onChange = {actualizarState}
                /> 

                <label>Telefono</label>
                <input
                   type = "text"
                    name = "telefono"
                    className = "u-full-width"
                    placeholder = "Telefono"
                /> 

                <label>Email</label>
                <input
                   type = "text"
                    name = "email"
                    className = "u-full-width"
                    placeholder = "Email"
                /> 
                <label>Direccion</label>
                <input
                   type = "text"
                    name = "direccion"
                    className = "u-full-width"
                    placeholder = "Direccion"
                /> 
                <button
                    type = "submit"
                    className = "u-full-width button-primary"
                >Aceptar</button>
            </form>
        </Fragment>
      );
}

export default Formulario;   